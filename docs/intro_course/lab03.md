> > _Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it._

Brian W. Kernighan

#### 3.1. Out-of-tree vs. In-tree drivers

Take the out-of-tree `hello_world` driver from the [template repo](https://gitlab.com/lkcamp/lkcamp_lab), and move it into `drivers/misc`, making it
an in-tree driver. Make sure it can be configured (enabled and disabled) and that it loads at boot time.

#### 3.2. Crushing bugs

Study the `yoda_buggy` driver. As it name implies, this driver has a few bugs. To begin with,
its implementation of `read(2)` does not follow POSIX semantics, so you'll have to fix that.
Find and fix as many bugs as you can.

??? note "Hints"
    * You can create a device node using `mknod`.
    * It's useful to work with a test-driven approach.
      You may use these commands:
        - cat /dev/yoda
        - dd if=/dev/yoda
        - dd if=/dev/yoda skip=15 bs=1
    * If you don't know what the commands do, the man pages are your friends: `man mknod`, `man dd`, etc.

In order to build the driver, here's a handy command:

```
make -C ~/PATH_TO_REPO M=`pwd` YOUR_DRIVER.ko
```

Feel free to fix the Makefile, or add a script to simplify the task.

#### 3.3. (Bonus) Permissions

What happens when `write(2)` is used on `yoda_buggy`?
Can you find where in the kernel code is this behaviour implemented?

#### 3.4. Fighting code duplication

Study the `simple_read_from_buffer()` kernel function. Can you take advantage of it to improve `yoda_buggy`?

#### 3.5. (Bonus) API design

Study Rusty Russell's design manifesto <http://sweng.the-davies.net/Home/rustys-api-design-manifesto>

#### 3.6. (Bonus) A more advanced char driver

Study the `fifo` driver. This driver has at least two bugs; find easy userspace reproducers for them
and fix the driver.

#### 3.7. Memory allocation in the kernel

Study the `clip` driver. This is a clipboard-like driver, not really POSIX-like. Therefore, in this case we don't care about being POSIX-compliant (it's just a little toy driver). That said, it has a serious bug. Can you find it?

??? note "Hints"
    * How many times can you write to the device?

#### 3.8. (Bonus) Dealing with leaks

Research and use kmemleak to detect the leaks in kernel drivers.

#### 3.9. Find the `struct device` for the RTC (real time clock) device. Find the `struct device_driver` that goes with it.

#### 3.10. Module parameters

Add a module parameter to the `yoda_buggy` driver, so it creates a number of devices configurable at module-insertion time.
