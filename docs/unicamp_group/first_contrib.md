**Talk**: how the community is organized, what is a maintainer, how the release cycle works

# (2) Sending a patch to the community

## (2.1) Getting dependencies to this Lab

??? note "For Ubuntu 16.04, Ubuntu 18.04, Debian Jessie or Debian Stretch"
    ```bash
    apt-get -y -q install          \
      bc                           \
      bison                        \
      build-essential              \
      codespell                    \
      curl                         \
      flex                         \
      git                          \
      git-email                    \
      iproute2                     \
      libauthen-sasl-perl          \
      libelf-dev                   \
      libmime-tools-perl           \
      libncurses-dev               \
      libssl-dev                   \
      libvirt-clients              \
      libvirt-daemon-system        \
      lsb-base                     \
      lsb-core                     \
      lsb-release                  \
      python3                      \
      python3-pip                  \
      qemu-kvm                     \
      qemu-utils                   \
      u-boot-tools                 \
      vim                          \
      wget                         \
      xz-utils
    ```

    The package list may be different on other Ubuntu versions, or other distributions.

## (2.2) Explore the kernel git repo

??? note "If you didn't download kernel repo follow this:"
    Clone the kernel git repo:

        git clone https://gitlab.ic.unicamp.br/lkcamp/linux-staging -b master --depth=1

        cd linux-staging

Update your kernel (if you had cloned it before):

    git pull

Identify yourself to commit in the right way:

    git config user.name "Danilo Rocha"
    git config user.email "your.email@domain.com"

See the branchs:

    git branch -a

Create a new branch to patch in the right way:

    git checkout -b first-patch

See the branchs again, and visualize your branch for patch:

    git branch -a

## (2.3) Setup vim

First, we need to make sure to enable the C indentation module in our default text editor (vim). Turning on this module will ensure that lines automatically get indented to the right level as you're editing. It saves you from hitting <tab> a lot. You can turn on automatic indentation based on the file type.

??? note "If you prefer configure .vimrc:"
    First run:

        vim ~/.vimrc

    Then add this line:

        filetype plugin indent on

    You'll also want to add a couple more lines, to turn syntax highlighting on, and show the file name in the terminal title bar:

        syntax on
        set title

    Most distributions compile vim so that 8 space tabs are the default. If you find they're not the default, you will need to add the following line to your .vimrc:

        set tabstop=8
        set softtabstop=8
        set shiftwidth=8
        set noexpandtab

??? note "If you prefer run command inside a vim opened file:"

    That's equivalent to running this command in vim:

        :set tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab

### (2.3.1) Setup vim as your default editor

??? note "update-alternatives:"
    Run command above and choose vim.basic or vim:

        sudo update-alternatives --config editor

??? note "variable ${EDITOR}:"
    First run:

        vim ~/.profile

    Then add this line:

        export EDITOR='vim'

## (2.4) Setup email

To be able to send Linux kernel patches, you'll need to be able to send email with your git.

Inside kernel repo folder run:

    vim .git/config

Then add this line:

    [sendemail]
        smtpEncryption = tls
        smtpServer = smtp.gmail.com
        smtpUser = your.email@gmail.com
        smtpServerPort = 587

!!! Warning "Beware with gmail secure"
    Gmail has a security feature that block e-mail send. To disable this feature access de URL above and turn off.

    <https://myaccount.google.com/lesssecureapps>

!!! Warning "2FA Gmail"
    If you have 2FA enabled in your Gmail follow this tip:

    If you have multifactor authentication setup on your gmail account, you will need to generate an app-specific password for use with git send-email. Visit <https://security.google.com/settings/security/apppasswords> to create it.

!!! Warning "If you use other e-mail domain"
    Adapt your configuration.



## (2.5) Hacking the kernel

First run:

    vim init/main.c

Then add this line above inside the function 'kernel_init(void \*unused)', after the line 'rcu_end_inkernel_boot()':

    rcu_end_inkernel_boot();
    printk(KERN_ERR "###################################\nI can hack the Linux kernel!\n###################################\n");

### (2.5.1) Compile your changes

??? note "If you are on full Virtual Machine or Physical Computer:"

        cp /boot/config-$(uname -r)\* .config
        make menuconfig
        make localmodconfig

        make -j$(($(nproc) - 1))

        sudo make modules_install install

        reboot

??? note "If you are on virtme:"

        virtme-configkernel --defconfig
        make oldconfig

        make -j$(($(nproc) - 1))

        virtme-run --kimg arch/x86/boot/bzImage

### (2.5.2) See your changes

Search for your printk():

    dmesg | less

### (2.5.3) Sending your first patch to yourself

Inside the kernel repo add your change to stage and commit:

    git add init/main.c

    git commit -m "My First Patch"

Run command and copy your commit ID:

    git log

Now, send to yourself your first patch:

    git send-email --annotate --suppress-cc=all HEAD^

## (2.6) checkpatch.pl

Linux kernel developers have very stringent guidelines for coding style. They're so picky, they created a script called [checkpatch.pl](http://lxr.free-electrons.com/source/scripts/checkpatch.pl) that you can run over your patches to make sure the patch complies with the kernel coding style.

### (2.6.1) Find a driver to clean up

    perl scripts/checkpatch.pl -f drivers/staging/greybus/* | less

!!! Warning "If you are doing this lab at IC3 room 322 follow this instructions:"
    <https://tree.taiga.io/project/rocha1558-lkcamp-m2/kanban>

### (2.6.2) Recompiling the driver

You'll need to make sure the driver you're changing is configured as a module. Run:

    make menuconfig

This opens up a text-based GUI that allows you to explore the configuration options.

Use the arrow keys to go to Device Drivers ->  and hit <enter>. Then go down to Staging drivers. At any time, you can hit '?', which will show you the help text for that kernel configuration option. You can search for the driver you're modifying by '/', in order to get the driver's longer name. Make sure the driver you're working on is compiled as a module ('M'), instead of being built-in ('\*'). You can change a driver to being compiled as a module by typing 'm' when the driver is selected in the menu. Hitting <enter> will change the driver to being built-in.

Once you've enabled the driver you're modifying, use <tab> or the right arrow key to move the cursor from 'Select' to 'Exit' and hit <enter>. Continue to do this until you get to the main menu. When it asks you to save your configuration, chose 'Yes'.

    make -j$(($(nproc) - 1))

You should reboot your kernel, load the driver with modprobe. You'll be able to see that the driver is loaded by running lsmod. Loading the driver at least makes sure that the driver probe function works.

!!! Warning "Note"
    Do not work on drivers that show that they depend on CONFIG_BROKEN. If you search for a driver after running `make menuconfig`, and you notice the "Depends on:" line includes BROKEN, do not work on this driver.

### (2.6.3) Sending your first patch to maintainer

Inside the kernel repo add your change to stage:

    git add <the_file_you_changed>

Then you run command below to commit on vim:

    git commit -s

Run command and copy your commit ID:

    git log

Checkpatch your patch first and solve the errors:

    perl scripts/checkpatch.pl -g <your_commit_id>

    or

    git show <your_commit_id> | perl scripts/checkpatch.pl

Search maintainer's email, in this case catch the mailing list e-mail:

    git show <your_commit_id> | perl scripts/get_maintainer.pl --nokeywords     \
                                                               --nogit          \
                                                               --nogit-fallback \
                                                               --norolestats

Check how the patch would be sent:

    git send-email --annotate --suppress-cc=all --dry-run <your_commit_id>^

Now, send to your first patch to maintainer:

    git send-email --annotate --suppress-cc=all <your_commit_id>^


## References

The content above was based on session *"Running checkpatch.pl"* from the tutorial at <https://kernelnewbies.org/Outreachyfirstpatch#FirstKernelPatch.Running_checkpatch.pl>.

## Done?

Congrats! You sent your first patch to the kernel \o/

Now you just need to wait for someone to review and some maintainer to accept your patch, then your modification will be in the next kernel release.

Now go back to what you did and try to check what you didn't understand. If you are done, try to help another student before moving on in this guide.

Don’t forget to update the spreadsheet for tracking our progress.
